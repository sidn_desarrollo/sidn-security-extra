# SIDN Extra Security

Este plugin añade algunas medidas de seguridad adicionales que no suelen estar incluidas en los plugins de seguridad.

Las medidas incluidas son las siguientes:

  - Core:
    - Ocultación de la versión de WordPress.
    - Ocultación de las versiones de estilos y scripts.
    - Ocultación de los errores de inicio de sesión.
  - Authors:
    - Deshabilitar la enumeración de usuarios por el parámetro ?author={id}.
    - Desactivar el endpoint de usuarios de la API REST a usuarios que no tengan la sesión iniciada.
    - Desactivar la enumeración de usuarios en el Sitemap.
    - Desactivar la enumeración de usuarios en oEmbed.
    - Desactivar archivos de autor.

En la versión actual del plugin no se permite desactivar ninguna característica, en futuras versiones conforme se añadan funcionalidades, se deberá de añadir un panel para poder activar y desactivar opciones.

El plugin cuenta con un actualizador que conecta con el repositorio para poder actualizarse como cualquier otro plugin de wordpress y evitar actualizaciones manuales.
