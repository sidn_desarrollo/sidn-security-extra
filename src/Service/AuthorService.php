<?php

namespace SIDN\SecurityExtra\Service;

if (!defined('ABSPATH')) { die(); }

class AuthorService implements Service {

  private static $instance;

  /**
   * Inicializa la clase
   */
  public static function init() {
    if(self::$instance === null) {
      self::$instance = new AuthorService();
    }

    return self::$instance;
  }

  // Inicializa los hooks
  public function __construct() {
    add_action('init', [$this, 'disable_author_enum_uri']);
    add_action('rest_authentication_errors', [$this, 'disable_author_rest_api_for_non_users']);
    add_action('wp_sitemaps_add_provider', [$this, 'disable_author_in_sitemap']);
    add_action('oembed_response_data', [$this, 'disable_author_in_oembed']);
    add_action('template_redirect', [$this, 'disable_author_archive']);
  }

 // Deshabilita la enumeración de usurios por url ?author=1 
  public function disable_author_enum_uri() {
    if (isset($_REQUEST['author']) && preg_match('/\\d/', $_REQUEST['author']) > 0) {
      wp_die('forbidden - number in author name not allowed.');
    }
  }

  // Deshabilita el acceso a la API REST de usuarios a usuarios externos.
  public function disable_author_rest_api_for_non_users($access) {
    if (is_user_logged_in()) {
      return $access;
    }

    if ((preg_match('/users/i', $_SERVER['REQUEST_URI']) !== 0) || (isset($_REQUEST['rest_route']) && (preg_match('/users/i', $_REQUEST['rest_route']) !== 0 ))) {
      return new \WP_Error('rest_cannot_access', 'You don\'t have sufficient permissions to access the user REST API.', ['status' => rest_authorization_required_code()]);
    }

    return $access;
  }

  // Deshabilita la enumeración de usuarios en el sitemap de WordPress.
  public function disable_author_in_sitemap($provider, $name) {
    $response = $provider;

    if ('users' === $name) {
      $response = false;
    }

    return $response;
  }

  // Deshabilita la enumeración de usuarios en oembed
  public function disable_author_in_oembed($data) {
    unset($data['author_url']);
    unset($data['author_name']);

    return $data;
  }

  // Deshabilitar archivos de autor
  public function disable_author_archive() {
    if (is_author() || isset($_GET['author'])) {
      wp_safe_redirect(esc_url(home_url('/' )), 301);
    }
  }

}