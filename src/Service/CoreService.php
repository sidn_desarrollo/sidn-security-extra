<?php

namespace SIDN\SecurityExtra\Service;

if (!defined('ABSPATH')) { die(); }

class CoreService implements Service {

  private static $instance;

  /**
   * Inicializa la clase
   */
  public static function init() {
    if(self::$instance === null) {
      self::$instance = new CoreService();
    }

    return self::$instance;
  }

  // Inicializa los hooks
  public function __construct() {
    add_action('login_errors', [$this, 'disable_login_errors']);
    add_action('the_generator', [$this, 'hide_wordpress_version']);
    add_action('style_loader_src', [$this, 'hide_script_and_style_version'], 9999);
    add_action('script_loader_src', [$this, 'hide_script_and_style_version'], 9999);
  }

  // Deshabilita los errores en el inicio de sesión
  public function disable_login_errors() {
    return 'Something is wrong!';
  }

  // Oculta la versión de WordPress
  public function hide_wordpress_version() {
    return '';
  }

  // Oculta la vesión de las dependencias de estilos y js
  public function hide_script_and_style_version($src) {
    if (strpos($src, 'ver=')) {
      $src = remove_query_arg('ver', $src);
    }

    return $src;
  }

}