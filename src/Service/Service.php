<?php

namespace SIDN\SecurityExtra\Service;

interface Service {
  public static function init();
  public function __construct();
}