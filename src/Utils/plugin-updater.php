<?php

if (!defined('ABSPATH')) { die(); }

namespace SIDN\SecurityExtra\Utils;

class GitLab_Updater {
	private $plugin_slug;
	private $version;
	private $update_url;

	public function __construct($plugin_slug, $version, $update_url) {
			$this->plugin_slug = $plugin_slug;
			$this->version = $version;
			$this->update_url = $update_url;

			add_filter('plugins_api', array($this, 'info'), 20, 3);
			add_filter('site_transient_update_plugins', array($this, 'update'));
			add_action('upgrader_process_complete', array($this, 'purge'), 10, 2);
	}

	public function info($res, $action, $args) {
			if ($action !== 'plugin_information' || $args->slug !== $this->plugin_slug) {
					return false;
			}

			$remote = $this->get_remote_info();

			if (!$remote) {
					return false;
			}

			$res = new stdClass();
			$res->name = $remote['name'];
			$res->slug = $this->plugin_slug;
			$res->version = $remote['version'];
			$res->tested = $remote['tested'];
			$res->requires = $remote['requires'];
			$res->author = '<a href="https://gitlab.com/tu-usuario">Tu Nombre</a>';
			$res->download_link = $remote['download_url'];
			$res->trunk = $remote['download_url'];
			$res->last_updated = $remote['last_updated'];

			return $res;
	}

	public function update($transient) {
			if (empty($transient->checked)) {
					return $transient;
			}

			$remote = $this->get_remote_info();

			if ($remote && version_compare($this->version, $remote['version'], '<')) {
					$res = new stdClass();
					$res->slug = $this->plugin_slug;
					$res->plugin = "{$this->plugin_slug}/{$this->plugin_slug}.php";
					$res->new_version = $remote['version'];
					$res->package = $remote['download_url'];

					$transient->response[$res->plugin] = $res;
			}

			return $transient;
	}

	public function purge($upgrader, $options) {
			if ($this->plugin_slug === $options['plugin']) {
					delete_transient('update_plugins');
			}
	}

	private function get_remote_info() {
			$response = wp_remote_get($this->update_url);

			if (is_wp_error($response)) {
					return false;
			}

			$remote = json_decode(wp_remote_retrieve_body($response), true);

			if (empty($remote)) {
					return false;
			}

			return $remote;
	}
}