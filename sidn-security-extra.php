<?php

/**
 * Plugin Name: SIDN Security Extra
 * Description: Complementa la seguridad de tu sitio web añadiendo algunas medidas de seguridad adicionales.
 * Version: 1.0.0
 * Requires at Least: 5.8
 * Requires PHP: 5.6
 * Author: SIDN
 * Author URI: https://www.sidn.es
 * Plugin URI: https://www.sidn.es
 * Update URI: https://gitlab.com/sidn_desarrollo/sidn-security-extra/-/raw/main/update-check.json?ref_type=heads
 */

if (!defined('ABSPATH')) { die(); }

// Importación del Autoload
require_once plugin_dir_path(__FILE__).'vendor/autoload.php';

// Importación de clases
use SIDN\SecurityExtra\Service\CoreService;
use SIDN\SecurityExtra\Service\AuthorService;

// Inicializamos las clases
CoreService::init();
AuthorService::init();
